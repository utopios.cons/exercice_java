package org.example.exercice3;

import java.util.Scanner;

public class Exercice3 {

    public  void solutionExo37() {
        int nombre = 0;
        double total = 0;
        System.out.println("Merci de saisir le nombre de photocopies à réaliser ?");
        Scanner s = new Scanner(System.in);
        nombre = s.nextInt();
        if (nombre > 20) {
            total = nombre * 0.05;
        } else if(nombre <= 20 && nombre >10){
            total = nombre * 0.10;
        }else{
            total = nombre * 0.15;
        }
        System.out.println("Le prix à payer est de" +" "+ total +" "+ "euros");
    }





}
