package org.example.exercice1;

public class Exercice {

    //Exercice 1
    private static void method1() {
        final String str = "java developers guide";
        String[] strArray = str.split(" ");
        System.out.println("Number of words in a string = " + strArray.length);
    }

    private static void method2() {
        final String str = "java developers guide";
        int count = 0;
        for (String word : str.split(" ")) {
            count++;
        }
        System.out.println("Number of words in a string = " + count);
    }


    //Exercice 2

    private static int countMatches(final String str, final char ch) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (ch == str.charAt(i)) {
                count++;
            }
        }
        return count;
    }


    // Exercice 3 : Anagramme

    static boolean isAnagram(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        } else {
            for (int i = 0; i < a.length(); i++) {
                char ch = a.toLowerCase().charAt(i);
                b = b.toLowerCase();
                if (b.indexOf(ch) != -1) {
                    b = b.replaceFirst(ch + "", "");
                } else {
                    return false;
                }
            }
            return b.length() == 0;
        }
    }

    // Palindrome version 1

    private static boolean isEmpty(final String cs) {
        return cs == null || cs.length() == 0;
    }

    public static boolean checkPalindrome(String input) {

        // Check error conditions
        if (isEmpty(input)) {
            return false;
        }
        String reverse = "";
        int length = input.length();

        for (int i = length - 1; i >= 0; i--) {
            reverse = reverse + input.charAt(i);
        }

        if (input.equals(reverse)) {
            System.out.println(input + " is palindrome = " + true);
        } else {
            System.out.println(input + " is palindrome = " + false);
        }
        return false;
    }

    // Palindrome version 2

    private static void checkPalindromeString(String input) {
        boolean result = true;
        int length = input.length();
        for (int i = 0; i < length / 2; i++) {
            if (input.charAt(i) != input.charAt(length - i - 1)) {
                result = false;
                break;
            }
        }
        System.out.println(input + " is palindrome = " + result);

    }

    //Palindrome version 3

    private static boolean isPalindrome(String str) {
        if (str == null)
            return false;
        StringBuilder strBuilder = new StringBuilder(str);
        strBuilder.reverse();
        return strBuilder.toString().equals(str);
    }



}
