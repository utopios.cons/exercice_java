package org.example.exercice4;

import java.util.Arrays;
import java.util.Scanner;

public class Exercice4 {


    private void afficheTabChar(char[] tableau) {
        System.out.println(Arrays.toString(tableau));
    }


    public void solutionExo510() {
        Integer prix = 1;
        Integer achat = 0;
        Integer paye = 0;
        Integer remise = 0;

        while (prix != 0) {
            System.out.println("Saisir le prix :");
            Scanner s6 = new Scanner(System.in);
            prix = s6.nextInt();
            System.out.println("prix :" + " " + prix);
            achat += prix;
            System.out.println("total :" + " " + achat);
        }
        System.out.println("Vous devez régler la somme de :" + " " + achat);
        System.out.println("Merci de saisir le montant de votre paiement");
        Scanner s7 = new Scanner(System.in);
        paye = s7.nextInt();
        remise = paye - achat;
        System.out.println("remise :" + " " + remise);


        while (remise / 10 >= 1) {
            System.out.println("10 euros");
            remise = remise - 10;
        }
        while (remise / 5 >= 1) {
            System.out.println("5 euros");
            remise = remise - 5;
        }
        while (remise / 1 >= 1) {
            System.out.println("1 euros");
            remise = remise - 1;
        }
    }
// Tableau décalage
    private void solutionExo54Tableau() {
        char[] tabDecalage = { 'D', 'E', 'C', 'A', 'L', 'A', 'G', 'E' };
        System.out.println("Le tableau :");
        afficheTabChar(tabDecalage);
        char temp = ' ';
        for (int i = 0; i < tabDecalage.length - 1; i++) {
            temp = tabDecalage[i];
            tabDecalage[i] = tabDecalage[i + 1];
            tabDecalage[i + 1] = temp;
        }
        System.out.println("Le tableau decalé :");
        afficheTabChar(tabDecalage);
    }


}
